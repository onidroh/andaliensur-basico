        <footer class="mastfoot mt-auto">
            <div class="inner">
                <p><img src="images/logoinfor.png" alt="Desarrollado en Unidad Informatica Andalién Sur" class="lazyload" width="70px" style="witdh:50px;"></p>
            </div>
        </footer>
    </div>
    
    <script src="js/jquery.min.js"></script>
    <script src="js/lazysizes.min.js" async></script>
    <script src="js/bootstrap.min.js" async></script>
    <script>
        $('.carousel').carousel({
            interval: 50000
        })
    </script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-157282406-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-157282406-1');
    </script>

</body>
</html>