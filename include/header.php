<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="theme-color" content="#563d7c">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>SLEP ~ Andalien Sur</title>
    <link rel="icon" type="image/webp" href="images/favicon.webp">
    <link rel="stylesheet" media="screen" href="css/bootstrap.min.css">
    <link rel="stylesheet" media="screen" href="css/estilo.css">
    <link rel="stylesheet" media="screen" href="css/font-awesome.min.css">
</head>
<body class="text-center">
    <div id="background-carousel">
        <div id="myCarousel" class="carousel slide" data-ride="carousel" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active"><img src="images/cap1.webp" class="rounded item-back"></div>
                <div class="carousel-item"><img src="images/cap2.webp" class="rounded lazyload item-back"></div>
                <div class="carousel-item"><img src="images/cap3.webp" class="rounded lazyload item-back"></div>
                <div class="carousel-item"><img src="images/cap4.webp" class="rounded lazyload item-back"></div>
                <div class="carousel-item"><img src="images/cap5.webp" class="rounded lazyload item-back"></div>
                <div class="carousel-item"><img src="images/cap6.webp" class="rounded lazyload item-back"></div>
                <div class="carousel-item"><img src="images/cap7.webp" class="rounded lazyload item-back"></div>
            </div>
        </div>
    </div>

    <div class="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column">
        <header class="masthead mb-auto">
            <div class="inner">
                <h3 class="masthead-brand"><img src="images/Logotipo.png" alt="" class="logo"></h3>
                <nav class="nav nav-masthead justify-content-center">
                    <a class="nav-link active" href="#">Home</a>
                    <a class="nav-link" href="http://intranetandalien.smc.cl/">Liquidaciones</a>
                    <a class="nav-link" href="https://mail.google.com/">Correo</a>
                </nav>
            </div>
        </header>