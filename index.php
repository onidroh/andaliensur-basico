<?php include 'include/header.php'; ?>

    <main role="main" class="inner cover">
        <h1 class="cover-heading" style="margin-bottom:1rem;">Ingresa al Sistema de Recursos Humanos.</h1>
        <div class="btn-group btn-block" role="group">
            <a role="button" href="http://intranetandalien.smc.cl/" class="btn btn-primary btn-lg btn-block">Intranet SMC</a>
            <a role="button" href="descarga/manualsmc.pdf" download="ManualSmc" class="btn btn-danger btn-md"><i class="fa fa-arrow-circle-o-down fa" aria-hidden="true"></i>MANUAL</a>
        </div>
    </main>

<?php include 'include/footer.php'; ?>